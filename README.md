Модуль Files для Yii2
=============================

Модуль для работы с файлами, изображениями.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-files": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-files.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'files' => []
    ],
```
