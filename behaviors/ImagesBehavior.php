<?php
namespace emilasp\files\behaviors;

use yii;
use yii\base\Exception;
use yii\caching\DbDependency;
use emilasp\files\models\File;
use emilasp\core\behaviors\relations\RelationBehavior;

/**
 * Class ImagesBehavior
 * @package emilasp\files\behaviors
 */
class ImagesBehavior extends RelationBehavior
{
    const RELATION_NAME = 'images';

    /** @var int тип файла отдаваемый по умолчанию */
    public $defaultType;

    public function init()
    {
        $this->relationClass = File::className();
        $this->relationName  = self::RELATION_NAME;
        $this->attribute = $this->relationName;
        parent::init();


    }


    /** Получаем одно изображение привязанное к моделе
     * @return array
     */
    public function getImage()
    {
        $images = $this->owner->images;
        if (count($images) > 0) {
            return $images[0];
        } else {
            return new File();
        }
        return $images;
    }

    /** Закрепляем изображение
     * @param $instance
     */
    public function addImage($instance = null)
    {
        $model = $this->owner;
        $file = new File();
        $file->name = (string)time();
        $file->object = $model::className();
        $file->object_id = $model->id;
        $file->type = File::TYPE_FILE_IMAGE;
        $file->status = 1;
        $file->save();
        $file->saveFile($instance);
    }
}
