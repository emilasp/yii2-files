<?php
namespace emilasp\files\behaviors;

use yii;
use yii\base\Exception;
use yii\caching\DbDependency;
use emilasp\files\models\File;
use emilasp\core\behaviors\relations\RelationBehavior;

/**
 * Class FileBehavior
 * @package emilasp\files\behaviors
 */
class FileBehavior extends RelationBehavior
{
    const RELATION_NAME = 'files';

    /** @var int тип файла отдаваемый по умолчанию */
    public $defaultType;

    public function init()
    {
        $this->relationClass = File::className();
        $this->relationName  = self::RELATION_NAME;
        $this->attribute = $this->relationName;
        parent::init();


    }

    /** Получаем файлы привязанные к моделе
     * @return array
     */
    public function getDocs()
    {
        $this->setCondition();
        return $this->getFilesByType(File::TYPE_FILE_FILE);
    }

    /** Получаем одно изображение привязанное к моделе
     * @return array
     */
    public function getImage()
    {
        $this->setCondition();
        $images = $this->owner->images;
        if (count($images) > 0) {
            return $images[0];
        } else {
            return new File();
        }
        return $images;
    }

    /** Получаем изображения привязанные к моделе
     * @return array
     */
    public function getImages()
    {
        $this->setCondition();
        return $this->getFilesByType(File::TYPE_FILE_IMAGE);
    }

    /** Закрепляем изображение
     * @param $instance
     */
    public function addImage($instance = null)
    {
        $model = $this->owner;
        $file = new File();
        $file->name = (string)time();
        $file->object = $model::className();
        $file->object_id = $model->id;
        $file->type = File::TYPE_FILE_IMAGE;
        $file->status = 1;
        $file->save();
        $file->saveFile($instance);
    }


    /** Отдаём файлы определёного типа
     *
     * @param $type
     *
     * @return array
     */
    private function getFilesByType($type)
    {
        $files = [];
        foreach ($this->owner->files as $file) {
            if ($file->type === $type) {
                $files[] = $file;
            }
        }
        return $files;
    }

    /**
     * Устанавливаем условия выборки для relation
     */
    private function setCondition()
    {
        $model           = $this->owner;
        $this->condition = [
            'object'    => $model::className(),
            'object_id' => $model->id,
        ];
    }
}
