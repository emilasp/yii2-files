<?php
namespace emilasp\files\behaviors;

use emilasp\core\behaviors\relations\RelationBehavior;
use yii;
use yii\base\Exception;
use yii\caching\DbDependency;
use emilasp\files\models\File;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

/**
 * Использование:
 *
 * Добавляем поведение
 * [
 *     'class' => FileSingleBehavior::className(),
 *  ],
 *
 * Добавляем виджет на форму
 * ImageInputWidget::widget([
 *  'form'      => $form,
 *  'model'     => $model,
 * ]);
 *
 * Class FileSingleBehavior
 * @package emilasp\files\behaviors
 */
class FileSingleBehavior extends RelationBehavior
{
    /** @var  Атрибут для формы*/
    public $upload;
    /** @var string имя атрибута загрузки файла */
    public $instanceAttr = 'upload';
    /** @var string наименование relation */

    public $relationName = 'image';
    /** @var string атрибут модели с id файла */
    public $attribute = 'image_id';
    /** @var bool Перезаписывать текущее изображение при смене */
    public $rewrite = true;

    public function init()
    {
        $this->relationClass = File::className();
        parent::init();
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveImage',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveImage',
        ];
    }

    /** Сохраняем изображение
     * @throws Exception
     */
    public function saveImage()
    {
        $image = UploadedFile::getInstance($this->owner, $this->instanceAttr);

        if ($image) {
            if ($this->rewrite) {
                $file = $this->rewriteImage($image);
            } else {
                $file = $this->addImage($image);
            }

            if ($file) {
                $this->owner->{$this->attribute} = $file->id;
                $this->owner->save();
            } else {
                throw new Exception("Image not save");
            }
        }
    }

    /**
     * Удаляем изображение
     */
    public function dropImage()
    {
        $oldFileId = $this->owner->oldAttributes[$this->attribute];

        if ($oldFileId) {
            $oldfile = File::findOne($oldFileId);
            if ($oldfile) {
                $oldfile->delete();
            }
        }
    }

    /**
     * Удаляем изображение
     */
    public function setEmptyImage()
    {
        if (!$this->owner->{$this->attribute} && isset($this->owner->oldAttributes[$this->attribute])) {
            $this->owner->{$this->attribute} = $this->owner->oldAttributes[$this->attribute];
        }
    }

    /** Закрепляем изображение
     * @param null $instance
     *
     * @return File
     */
    public function addImage($instance)
    {
        $model           = $this->owner;
        $file            = new File();
        $file->name      = (string)time();
        $file->attribute = $this->attribute;
        $file->object    = $model::className();
        $file->object_id = $model->id;
        $file->type      = File::TYPE_FILE_IMAGE;
        $file->status    = 1;
        $file->save();
        $file->saveFile($instance);

        return $file;
    }

    /** Закрепляем изображение
     * @param null $instance
     *
     * @return File
     */
    public function rewriteImage($instance)
    {
        $file = null;
        if ($this->owner->{$this->attribute}) {
            $file = File::findOne($this->owner->{$this->attribute});
        }


        if ($file) {
            $file->saveFile($instance);
        } else {
            $file = $this->addImage($instance);
        }
        return $file;
    }
}
