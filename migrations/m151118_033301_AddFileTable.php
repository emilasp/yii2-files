<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;

class m151118_033301_AddFileTable extends Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('files_file', [
            'id'         => $this->primaryKey(11),
            'title'      => $this->string(255),
            'name'       => $this->string(255)->notNull(),
            'type'       => $this->string(14)->notNull(),
            'params'     => $this->string(255),
            'object'     => $this->string(128),
            'object_id'  => $this->integer(11),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_files_file_created_by',
            'files_file',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_files_file_updated_by',
            'files_file',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('files_model', 'files_file', ['object', 'object_id', 'status']);
    }

    public function down()
    {
        $this->dropTable('files_file');
        return true;
    }

}
