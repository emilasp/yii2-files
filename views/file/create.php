<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\files\models\File */

$this->title = Yii::t('files', 'Create File');
$this->params['breadcrumbs'][] = ['label' => Yii::t('files', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
