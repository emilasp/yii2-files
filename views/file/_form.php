<?php

use dosamigos\fileupload\FileUpload;
use emilasp\files\models\File;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\files\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin([
        'id' => 'file-form-id',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'file')->fileInput(['id' => 'file-upload', 'class' => 'btn btn-success']) ?>
        </div>

        <div class="col-md-8 text-right">
            <?php if (!$model->isNewRecord) : ?>
                <img src="<?= $model->getUrl(File::SIZE_MED) ?>"
            <?php endif ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model::$types) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'params')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'attribute')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'object_id')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'object')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$js = <<<JS
$('#file-upload').uploadPreview();
JS;

$this->registerJs($js);