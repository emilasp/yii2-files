<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use emilasp\files\models\File;
use emilasp\user\core\models\User;
use emilasp\files\extensions\columns\FileColumn;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\files\models\search\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('files', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute'     => 'file',
                'class'         => FileColumn::className(),
                'width'         => '100px',
                'hAlign'        => GridView::ALIGN_CENTER,
                'vAlign'        => GridView::ALIGN_MIDDLE,
            ],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'name',
            'title',
            [
                'attribute' => 'type',
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => File::$types,
            ],
            'object',
            'object_id',
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->statuses,
            ],
            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
            ],
        ],
        'responsive'   => true,
        'hover'        => true,
        'condensed'    => true,
        'floatHeader'  => true,
        'panel'        => [
            'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
            'type'       => 'info',
            'before'     => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('site', 'Add'),
                ['create'],
                ['class' => 'btn btn-success']
            ),
            'after'      => Html::a(
                '<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('site', 'Reset List'),
                ['index'],
                ['class' => 'btn btn-info']
            ),
            'showFooter' => false,
        ],
    ]);
    ?>

</div>
