<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\files\models\File */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('files', 'File'),
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('files', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
