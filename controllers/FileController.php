<?php

namespace emilasp\files\controllers;

use emilasp\core\extensions\jsHelpers\JsHelpersAsset;
use emilasp\core\helpers\UrlHelper;
use emilasp\variety\models\Variety;
use stdClass;
use Yii;
use emilasp\files\models\File;
use emilasp\files\models\search\FileSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view', 'create', 'update', 'delete', 'delete-ajax'],
                'rules'        => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-ajax'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /** Upload image BlueImp jQuery File Upload plugin
     * @return string
     */
    public function actionUpload()
    {
        $file = UploadedFile::getInstanceByName('image-upload');

        $object    = UrlHelper::classDecode(Yii::$app->request->get('object'));
        $id        = Yii::$app->request->get('id');
        $attribute = Yii::$app->request->get('attribute');

        $model            = new File();
        $model->name      = (string)time();
        $model->status    = Variety::getValue('status_enabled');
        $model->type      = File::TYPE_FILE_IMAGE;
        if($attribute) {
            $model->attribute    = $attribute;
        }
        $model->object    = $object;
        $model->object_id = $id;
        $model->save();
        $model->saveFile($file);

        $fileArray                  = [];
        $fileArray['url']           = $model->getUrl(File::SIZE_ORG);
        $fileArray['thumbnail_url'] = $model->getUrl(File::SIZE_ORG);
        $fileArray['delete_url']    = $model->getUrl(File::SIZE_ORG);
        $fileArray['delete_type']   = 'DELETE';
        $fileArray['name']          = $model->name;
        $fileArray['size']          = '7419';

        $filejson          = new stdClass();
        $filejson->files[] = $fileArray;

        echo Json::encode($filejson);
        Yii::$app->end();
    }


    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single File model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, File::className()),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new File();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, File::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDeleteAjax()
    {
        $id = Yii::$app->request->post('id');

        $model = $this->findModel($id, File::className());
        $model->delete();

        return JsHelpersAsset::response(1, '1', '0');
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, File::className())->delete();

        return $this->redirect(['index']);
    }
}
