<?php
use dosamigos\fileupload\FileUploadUI;
use emilasp\core\helpers\UrlHelper;
use emilasp\files\models\File;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

$object = UrlHelper::classEncode($model::className());
?>

<?= FileUploadUI::widget([
    'name'          => 'image-upload',
    'url'           => ['/files/file/upload', 'id' => $model->id, 'object' => $object, 'attribute' => $attribute],
    'gallery'       => false,
    'fieldOptions'  => [
        'accept' => 'image/*',
    ],
    'clientOptions' => [
        'maxFileSize' => 2000000,
    ],
    'clientEvents'  => [
        'fileuploaddone' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                    $.pjax({
                                        container:"#files-blocks",
                                        "timeout" : 0,
                                        //url: "/strategic/task/update-ajax",
                                        push:false
                                    });
                                    return true;
                                }',
        'fileuploadfail' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                }',
    ],
]);
?>

<?php Pjax::begin(['id' => 'files-blocks']); ?>

<?php foreach ($model->images as $image) : ?>

    <div class="image-manager-img-block">
        <div class="row image-block-tools">
            <div class="col-xs-6">
                <button type="button" class="btn btn-primary btn-update" data-id="<?= $image->id ?>">
                    <i class="fa fa-edit"></i>
                </button>
            </div>
            <div class="col-xs-6 text-right">
                <button type="button" class="btn btn-danger btn-delete" data-id="<?= $image->id ?>">
                    <i class="fa fa-ban"></i>
                </button>
            </div>
        </div>
        <img class="image-block" src="<?= $image->getUrl(File::SIZE_ICO, true) ?>"/>
        <div class="description-image"><?= $image->title ?></div>
    </div>

<?php endforeach ?>

<?php Pjax::end(); ?>

<?php
$urlToDeleteFile = \yii\helpers\Url::toRoute(['/files/file/delete-ajax']);
$urlToSendFile   = \yii\helpers\Url::current();
$js              = <<<JS
    $('#file-upload').uploadPreview();
    $('.btn-ajax-modal').click(function (){
        $('#modal_file').modal('show');
    });

    $('body').on('click', '.btn-ajax-save-file', function(){
        var id = $(this).data('id');
        var form = $('#file-form-id'); // You need to use standart javascript object here
        if (window.FormData){
            formdata = new FormData(form[0]);
            formdata.append('action', 'update-file-send');
            formdata.append('idfile', id);
        }

        $.ajax({
            url: '{$urlToSendFile}',
            type: 'POST',
            dataType: "json",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(msg) {
                if(msg[0]=='1'){
                    notice(msgTrue, 'green');
                }else{
                    notice(msgFalse, 'red');
                }
            },
            error: function(msg){
                $('#modal-file-form').modal('hide');
                notice('good', 'green');
                $.pjax({
                    container:"#files-blocks",
                    "timeout" : 0,
                    push:false
                });
            }
        });
    });

    $('body').on('click', '.btn-delete', function(){
        if (confirm('Удалить файл?')) {
            var id = $(this).data('id');
            ajax('{$urlToDeleteFile}', {'id':id}, 'Успешно.', 'Не удалось удалить', function () {
                $.pjax({
                    container:"#files-blocks",
                    "timeout" : 0,
                    push:false
                });
            });
        }
    });
    $('body').on('click', '.btn-update', function(){
        var id = $(this).data('id');
        $('#modal-file-form').modal('show');
        $.pjax({
            container:"#files-form-pjax",
            "timeout" : 0,
            type: 'POST',
            data:{action:'update-file',idfile:id},
            push:false,
        });

    });
JS;

$this->registerJs($js);
