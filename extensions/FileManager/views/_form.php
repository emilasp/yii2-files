<?php

use dosamigos\fileupload\FileUpload;
use emilasp\files\models\File;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model emilasp\files\models\File */
/* @var $form yii\widgets\ActiveForm */
?>


<?php Modal::begin([
    'id'     => 'modal-file-form',
    'header' => '<h4>' . Yii::t('taxonomy', 'Group') . '</h4>',
]); ?>
<?php Pjax::begin(['id' => 'files-form-pjax']); ?>



    <div class="file-form">

        <?php $form = ActiveForm::begin([
            'id' => 'file-form-id',
            'options' => ['enctype'=>'multipart/form-data']
        ]); ?>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($fileModel, 'title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?php if (!$fileModel->isNewRecord) : ?>
            <img src="<?= $fileModel->getUrl(File::SIZE_MED, true) ?>"
        <?php endif ?>
        <?= $form->field($fileModel, 'file')->fileInput(['id' => 'file-upload', 'class' => 'btn btn-success'])->label(false) ?>

        <div class="form-group">
            <?= Html::button(Yii::t('site', 'Save'), [
                'class' => 'btn btn-success btn-ajax-save-file',
                'data-id' => $fileModel->id
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php Pjax::end(); ?>
<?php Modal::end(); ?>

<?php
$js = <<<JS
$('#file-upload').uploadPreview();
JS;

$this->registerJs($js);