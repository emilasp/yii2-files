<?php
namespace emilasp\files\extensions\FileManager;

use yii\web\AssetBundle;

/**
 * Class FileManagerAsset
 * @package emilasp\files\extensions\FileManager
 */
class FileManagerAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = [];
    public $css = ['list.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
