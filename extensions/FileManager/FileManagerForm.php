<?php
namespace emilasp\files\extensions\FileManager;

use emilasp\core\components\base\Widget;
use emilasp\files\models\File;
use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Выводим форму для загрузки нового файла
 *
 * Class FileManager
 * @package emilasp\files\extensions\FileManager
 */
class FileManagerForm extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $fileModel = $this->isUpdateRequest();
        echo $this->render('_form', [
            'fileModel' => $fileModel,
            'model' => $this->model
        ]);
    }

    /**
     * @return File|null|static
     */
    private function isUpdateRequest()
    {
        $action = Yii::$app->request->get('action', Yii::$app->request->post('action'));
        $id = Yii::$app->request->get('idfile', Yii::$app->request->post('idfile'));
        if ($action === 'update-file') {
            $model = File::findOne($id);
        } elseif ($action === 'update-file-send') {
            $model = File::findOne($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
            }
        } else {
            $model = new File();
        }
        return $model;
    }
}
