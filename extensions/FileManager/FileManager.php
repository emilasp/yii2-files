<?php
namespace emilasp\files\extensions\FileManager;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Виджет управления приаттаченными изображениями
 *
 * Class FileManager
 * @package emilasp\files\extensions\FileManager
 */
class FileManager extends Widget
{
    public $model;
    public $form;
    public $files;
    public $attribute;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('list', [
            'form'      => $this->form,
            'model'     => $this->model,
            'attribute' => $this->attribute,
        ]);
    }

    private function registerAssets()
    {
        FileManagerAsset::register($this->view);
    }
}
