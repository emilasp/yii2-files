<?php
namespace emilasp\files\extensions\columns;

use emilasp\core\components\base\ActiveRecord;
use yii\helpers\Html;
use kartik\grid\DataColumn;
use emilasp\files\models\File;

/**
 * ImageColumn displays a column of row numbers (1-based).
 * @author Emilasp
 * @since 1.0
 */
class FileColumn extends DataColumn
{
    public $attribute;
    public $header = '--';
    public $size   = '30';

    public $linkAttribute;

    /**
     * @param ActiveRecord $model
     * @param mixed $key
     * @param int $index
     *
     * @return null|string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->linkAttribute) {
            $model = $model->{$this->linkAttribute};
        }
        if (!$model || !is_file($model->getFilePath(File::SIZE_ICO))) {
            $model = new File();
        }
        $url = $model->getUrl(File::SIZE_ICO);

        return Html::a(
            Html::img(
                $url,
                [
                    'height' => $this->size,
                    'class'  => 'cursor-pointer',
                    'data'   => ['jbox-image' => 'gallery-cert'],
                ]
            ),
            ['update', 'id' => $model->id],
            ['data-pjax' => 0, 'target' => '_blank',]
        );
    }
}
