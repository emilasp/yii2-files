<?php
namespace emilasp\files\extensions\imageInputWidget;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Виджет управления приаттаченными изображениями
 *
 * Class ImageInputWidget
 * @package emilasp\files\extensions\imageInputWidget
 */
class ImageInputWidget extends Widget
{
    public $model;
    public $form;
    public $relationName = 'image';
    public $instanceAttr = 'upload';
    public $preview;

    public function init()
    {
        parent::init();

        if ($this->model->{$this->relationName}) {
            $this->preview = $this->model->{$this->relationName}->getUrl('min');
        }
    }

    public function run()
    {
        echo $this->render('field', [
            'form'      => $this->form,
            'model'     => $this->model,
            'attribute' => $this->instanceAttr,
            'preview'   => $this->preview,
        ]);
    }
}
