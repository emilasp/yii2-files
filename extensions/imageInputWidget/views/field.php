<?php
use kartik\widgets\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<?php
$pluginOptions = [
    'showCaption' => false,
    'showRemove'  => false,
    'showUpload'  => false,
    'browseClass' => 'btn btn-primary btn-block',
    'browseIcon'  => '<i class="glyphicon glyphicon-camera"></i> ',
    'browseLabel' => 'Upload Receipt',
];

if ($preview) {
    $pluginOptions['initialPreview']   = Html::img($preview, [
        'class' => 'file-preview-image',
        'alt'   => 'The Moon',
        'title' => 'The Moon',
    ]);
    $pluginOptions['overwriteInitial'] = true;
}
?>

<?= FileInput::widget([
    'model'         => $model,
    'attribute'     => $attribute,
    'options'       => ['multiple' => false],
    'pluginOptions' => $pluginOptions,
    'options'       => ['accept' => 'image/*'],
])
?>
