<?php
namespace emilasp\files\helpers;

use Yii;
use yii\log\Logger;
use ErrorException;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\base\Exception;
use Imagine\Image\Point;
use Imagine\Image\BoxInterface;
use Imagine\Image\ImageInterface;
use Imagine\Image\ManipulatorInterface;

/**
 * Class FileModuleHelper
 * @package emilasp\files\helpers
 */
class FileModuleHelper
{
    /** Получаем ширину и высоту
     * @param $img
     * @param $params
     *
     * @return array
     */
    public static function getSizes($img, $params)
    {
        $size   = $img->getSize();

        if (empty($params['size']) || strpos($params['size'], 'x') === false) {

            $width  = $size->getWidth();
            $height = $size->getHeight();
        } else {
            $sizes  = explode('x', $params['size']);
            $width  = $sizes[0];
            $height = $sizes[1];

            if (!$height && $width) {
                $orgWidth = $size->getWidth();
                $orgHeight = $size->getHeight();

                $percent = $size->getWidth()/$width;
                $height = ceil($size->getHeight()/$percent);
            }

        }
        return ['width' => $width, 'height' => $height];
    }
    
    /** Метод сохраняет имеющееся изображение в новом формате/размере по указанному пути
     *
     * @param $original
     * @param $cachePathFile
     * @param $params
     * @param $deleteOriginal
     *
     * @return bool
     * @throws ErrorException
     */
    public static function saveImage($original, $cachePathFile, $params, $deleteOriginal = true)
    {
        try {
            $imagine = Image::getImagine();
            $img     = $imagine->open(Yii::getAlias($original));

            $sizes = self::getSizes($img, $params);
            $width = $sizes['width'];
            $height = $sizes['height'];

            if (!empty($params['crop'])) {
                $thumb = self::getImageCrop($img, $width, $height);
            } elseif (!empty($params['aspectRatio'])) {
                $thumb = self::getImageAspectRatio($img, $width, $height);
            } else {
                $width  = empty($width) ? 2000 : $width;
                $height = empty($height) ? 2000 : $height;

                $thumb = self::getImageRatio($img, $width, $height);
            }

            if (!empty($params['watermark']) && $thumb->getSize()->getWidth() > 200) {
                $watermark = Image::getImagine()->open(Yii::getAlias($params['watermarkSrc']));
                $scale     = $params['scaleWatermark'];
                $watermark = self::getImageRatio(
                    $watermark,
                    $thumb->getSize()->getWidth() / $scale,
                    $thumb->getSize()->getHeight() / $scale
                );
                $thumb->paste($watermark, new Point(10, 10));
            }

            $thumb->save($cachePathFile, ['quality' => $params['quality']]);

            if ($deleteOriginal) {
                unlink($original);
            }
        } catch (Exception $e) {
            //\Yii::getLogger()->log('Error save Image: ' . var_dump($e), Logger::LEVEL_ERROR, 'binary');
            return false;
        }
        return true;
    }

    /**
     * Метод возвращает отресайженное по максимальному ратио и откропленое по размеру изображение
     * в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageCrop($img, $width, $height)
    {
        $size          = $img->getSize();
        $ratio         = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight(), true);
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        $_width  = ($img->getSize()->getWidth() + $width) / 2 - $img->getSize()->getWidth();
        $_height = ($img->getSize()->getHeight() + $height) / 2 - $img->getSize()->getHeight();

        $img->crop(new Point(abs($_width), abs($_height)), new Box($width, $height));
        return $img;
    }


    /**
     * Метод возвращает отресайженое изображение в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageAspectRatio($img, $width, $height)
    {
        $imagine   = Image::getImagine();
        $size      = new Box($width, $height);
        $mode      = ImageInterface::THUMBNAIL_INSET;
        $resizeimg = $img->thumbnail($size, $mode);

        $sizeR   = $resizeimg->getSize();
        $widthR  = $sizeR->getWidth();
        $heightR = $sizeR->getHeight();

        $preserve = $imagine->create($size);
        $startX   = $startY = 0;
        if ($widthR < $width) {
            $startX = ($width - $widthR) / 2;
        }
        if ($heightR < $height) {
            $startY = ($height - $heightR) / 2;
        }
        $preserve->paste($resizeimg, new Point($startX, $startY));
        return $preserve;
    }

    /**
     * Метод возвращает отресайженое изображение в виде объекта ImageInterface
     *
     * @param $img ImageInterface
     * @param $width integer
     * @param $height integer
     *
     * @return ImageInterface
     */
    public static function getImageRatio($img, $width, $height)
    {
        $size          = $img->getSize();
        $ratio         = self::inset(new Box($width, $height), $size->getWidth(), $size->getHeight());
        $thumbnailSize = $size->scale($ratio);

        $img->resize($thumbnailSize);

        return $img;
    }

    /**
     * Считаем ратио - ресайзим изображение так что бы оно влазило в прямоугольник, но сохраняло при этом пропорции.
     *
     * @param BoxInterface $size
     * @param integer $_width
     * @param integer $_height
     * @param bool $outset
     *
     * @return mixed
     */
    public static function inset(BoxInterface $size, $_width, $_height, $outset = false)
    {
        $width  = $size->getWidth();
        $height = $size->getHeight();

        if ($outset) {
            $ratio = max([
                $width / $_width,
                $height / $_height,
            ]);
        } else {
            $ratio = min([
                $width / $_width,
                $height / $_height,
            ]);
        }
        return $ratio;
    }
}
