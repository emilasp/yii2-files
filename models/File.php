<?php

namespace emilasp\files\models;

use Yii;
use ReflectionClass;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\base\ErrorException;
use emilasp\user\core\models\User;
use emilasp\core\helpers\UrlHelper;
use emilasp\core\helpers\FileHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Imagine\Image\ManipulatorInterface;
use emilasp\files\helpers\FileModuleHelper;
use emilasp\core\components\base\ActiveRecord;
use emilasp\variety\behaviors\VarietyModelBehavior;

/**
 * This is the model class for table "files_file".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $type
 * @property string $params
 * @property string $attribute
 * @property string $object
 * @property integer $object_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class File extends ActiveRecord
{
    /** Типы файлов */
    const TYPE_FILE_IMAGE = 'image';
    const TYPE_FILE_XLS   = 'xls';
    const TYPE_FILE_DOC   = 'doc';
    const TYPE_FILE_FILE  = 'file';

    /** Стандартные размеры */
    const SIZE_ICO = 'ico';
    const SIZE_MIN = 'min';
    const SIZE_MID = 'mid';
    const SIZE_MED = 'med';
    const SIZE_MAX = 'max';
    const SIZE_ORG = 'org';

    /** @var array Типы файла */
    public static $types = [
        self::TYPE_FILE_IMAGE => self::TYPE_FILE_IMAGE,
        self::TYPE_FILE_FILE  => self::TYPE_FILE_FILE,
        self::TYPE_FILE_DOC   => self::TYPE_FILE_DOC,
        self::TYPE_FILE_XLS   => self::TYPE_FILE_XLS,
    ];

    /** @var string файл из формы */
    public $file;

    /** @var string раширение файла */
    private $ext;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files_file';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['object_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'name', 'params'], 'string', 'max' => 255],
            [['type', 'attribute'], 'string', 'max' => 14],
            [['object'], 'string', 'max' => 128],

            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'title'      => Yii::t('site', 'Title'),
            'name'       => Yii::t('site', 'Name'),
            'type'       => Yii::t('site', 'Type'),
            'params'     => Yii::t('site', 'Params'),
            'attribute'  => Yii::t('site', 'Attribute'),
            'object'     => Yii::t('site', 'Object'),
            'object_id'  => Yii::t('site', 'Object ID'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /** Устанавливаем имя файла
     *
     * @param $name
     *
     * @return mixed|string
     */
    public function getSlug($name)
    {
        if (!$this->name) {
            if (!$this->owner) {
                $name = time();
            }
        }
        $this->name = UrlHelper::strTourl($name);
        return $this->name;
    }

    /** Устанавливаем имя файла
     *
     * @param $name
     */
    public function setName($name)
    {
        $this->name = UrlHelper::strTourl($name);
    }

    /** Получаем расширение в зависимости от типа файла
     * @return mixed
     */
    public function getExt()
    {
        if (!$this->ext) {
            if ($this->type === self::TYPE_FILE_IMAGE) {
                $this->ext = '.jpg';
            } else {
                $this->ext = '';
            }
        }
        return $this->ext;
    }

    /** Получаем относительный url до файла
     *
     * @param null $prefix
     *
     * @return mixed
     */
    public function getUrl($prefix = 'org', $noCache = false, $scheme = '')
    {
        /** Если это новая модель и нет изображения */
        if ($this->isNewRecord) {
            $pathNoImage = Yii::$app->getModule('files')->config['noImage']['path'];
            $fileNoImage = $prefix . Yii::$app->getModule('files')->config['noImage']['file'];

            return $pathNoImage . $fileNoImage;
        }

        $path = $this->getFilePath($prefix, false);

        if (!$this->checkFileInCache($this->getFilePath($prefix))) {
            $this->saveImageSizes();
        };

        if ($noCache) {
            $path .= '?expired=' . time();
        }

        $url = Yii::$app->getModule('files')->webBasePathFiles . $path;

        if ($scheme) {
            return rtrim(Url::to(['/'], true), '/') . $url;
        } else {
            return $url;
        }
    }

    /** Получаем полный путь до файла
     *
     * @param string $prefix
     * @param bool|true $absolute
     *
     * @return string
     */
    public function getFilePath($prefix = '', $absolute = true)
    {
        return $this->getPath($absolute) . DIRECTORY_SEPARATOR . $prefix . $this->name . $this->getExt();
    }

    /** Получаем полный путь до папки с файлом в кеше
     *
     * @param bool|true $absolute
     *
     * @return string
     */
    public function getPath($absolute = true)
    {
        $folder    = 'file';
        $subFolder = $this->id;
        if ($this->object) {
            $folder = ucfirst((new ReflectionClass($this->object))->getShortName());

            if ($this->object_id) {
                $subFolder = $this->object_id . DIRECTORY_SEPARATOR . $this->id;
            }
        }

        $basePath = '';
        if ($absolute) {
            $basePath = Yii::getAlias(Yii::$app->getModule('files')->pathCache);
        }
        return $basePath . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $subFolder;
    }

    /** Получаем полный путь до файла оригинала
     *
     * @param bool|false $withFile
     * @param bool|false $withExt
     *
     * @return string
     */
    public function getPathToOriginal($withFile = true, $withExt = true)
    {
        $basePath = Yii::getAlias(Yii::$app->getModule('files')->pathOriginal);

        $fileName = '';
        if ($withFile) {
            $fileName = $this->getSlug($this->name);
            if ($withExt) {
                $fileName .= $this->getExt();
            }
        }

        return $basePath . DIRECTORY_SEPARATOR
               . $this->id . DIRECTORY_SEPARATOR
               . $fileName;
    }

    /** Проверяем что файл уже создан и если нет, то создаём его
     *
     * @param $path
     *
     * @return bool
     */
    private function checkFileInCache($path)
    {
        return is_file($path);
    }

    /** Сохраняем файл, если передан аргумент $url, то из интернета, если нет, то из формы
     *
     * @param string|false|UploadedFile $instance
     *
     * @return bool|null|string
     */
    public function saveFile($instance = false)
    {
        $fileSave         = null;
        $pathOriginal     = $this->getPathToOriginal(false);
        $pathOriginalFile = $this->getPathToOriginal(true, false);

        FileHelper::createDirectory($pathOriginal);
        if ($instance instanceof UploadedFile) {
            $file = $instance;
            if ($file) {
                $file->saveAs($pathOriginalFile);
            }
        } elseif ($instance && UrlHelper::isUrl($instance)) {
            FileHelper::saveFileFromInternet($instance, $pathOriginal, $pathOriginalFile);
        } else {
            $file = UploadedFile::getInstance($this, 'file');
            if ($file) {
                $file->saveAs($pathOriginalFile);
            }
        }
        if (is_file($pathOriginalFile)) {
            $this->saveOriginal();
            $fileSave = true;
        }
        return $fileSave;
    }

    /** Сохраняем оригинал
     * @throws \ErrorException
     */
    private function saveOriginal()
    {
        $params             = Yii::$app->getModule('files')->getConfig($this->object, $this->attribute);
        $pathOriginal       = $this->getPathToOriginal(true, false);
        $pathOriginalTarget = $this->getPathToOriginal();

        FileModuleHelper::saveImage(
            $pathOriginal,
            $pathOriginalTarget,
            $params[self::SIZE_ORG]
        );

        FileHelper::removeDirectory($this->getPath());
    }

    /**
     * Сохраняем размеры изображения
     */
    public function saveImageSizes()
    {
        $original = $this->getPathToOriginal();

        if (!is_file($original)) {
            return;
            //throw new ErrorException('Not found original image');
        }

        $imgConfig = Yii::$app->getModule('files')->getConfig($this->object, $this->attribute);

        FileHelper::removeDirectory($this->getPath());
        FileHelper::createDirectory($this->getPath());

        foreach ($imgConfig as $prefix => $params) {
            $cachePathFile = $this->getFilePath($prefix);
            FileModuleHelper::saveImage($original, $cachePathFile, $params, false);
        }
    }

    /**
     * @param bool $insert
     *
     * @return bool|null|string
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if ($this->owner) {
                    if (isset($this->owner->name)) {
                        $this->name = $this->getSlug($this->owner->name);
                    } elseif (isset($this->owner->title)) {
                        $this->name = $this->getSlug($this->owner->title);
                    }
                    if (!$this->title) {
                        if (isset($this->owner->title)) {
                            $this->title = $this->owner->title;
                        } elseif (isset($this->owner->name)) {
                            $this->title = $this->owner->name;
                        }
                    }
                }
            } else {
                if ($this->title && $this->title !== $this->getOldAttribute('title')) {
                    $this->name = $this->getSlug($this->title);
                }
                /** Если файл не загружен, но сменился title и name, то переименовываем оригинал */
                if (!$this->saveFile() && $this->name !== $this->getOldAttribute('name')) {
                    $oldFile      = $this->getOldAttribute('name');
                    $pathOriginal = $this->getPathToOriginal(false, false);

                    $oldOriginalFile = $pathOriginal . $oldFile . $this->getExt();
                    $newOriginalFile = $pathOriginal . $this->name . $this->getExt();
                    rename($oldOriginalFile, $newOriginalFile);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     *
     * @return bool|null|string
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->saveFile();
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $this->deleteFiles();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        if ($this->object) {
            return $this->hasOne($this->object, ['id' => 'object_id']);
        }
        return null;
    }

    /** Удаляем файлы модели
     * @throws ErrorException
     */
    private function deleteFiles()
    {
        $originalPath = $this->getPathToOriginal(false);
        $cachePath    = $this->getPath();

        if (is_dir($originalPath)) {
            FileHelper::removeDirectory($originalPath);
        }
        if (is_dir($cachePath)) {
            FileHelper::removeDirectory($cachePath);
        }
    }
}
