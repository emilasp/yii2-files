<?php
namespace emilasp\files;

use emilasp\core\CoreModule;
use yii\helpers\ArrayHelper;

/**
 * Class SiteModule
 * @package emilasp\files
 */
class FileModule extends CoreModule
{
    /** @var array  */
    public $config = [
        /*'waterMark'      => '@common/media/images/watermark.png',
        'quality' => 100,
        'scaleWaterMark' => 5,*/
    ];

    public $sizes = [
        /* Product::className() => [
             'min_' => [
                 'size' => '100x100',
                 'watermark' => true,
                 'crop' => true
             ]*/
    ];

    /** @var string Папка где мы храним оригиналы */
    public $pathOriginal = '@common/media/uploads/protect/files/originals';

    /** @var string Папка в которой мы храним сгенерированные размеры */
    public $pathCache = '@common/media/uploads/web/media/files';

    /** Родительская папка в @web с файлами */
    public $webBasePathFiles = '/media/files';



    /** Получаем конфиг для изображения
     * @param string $object
     * @param string $attribute
     * @return mixed
     */
    public function getConfig($object = null, $attribute = null)
    {
        if ($object && $attribute && isset($this->sizes[$object][$attribute])) {
            $sizes = ArrayHelper::merge($this->sizes['default'], $this->sizes[$object][$attribute]);
        } elseif ($object && isset($this->sizes[$object])) {
            $sizes = ArrayHelper::merge($this->sizes['default'], $this->sizes[$object]);
        } else {
            $sizes = $this->sizes['default'];
        }
        
        $sizes = array_map(function ($size) {
            return ArrayHelper::merge($this->config, $size);
        }, $sizes);
        return $sizes;
    }
}
